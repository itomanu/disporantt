<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
	public function __construct(){
		parent::__construct();

		$this->load->model('Wasit');
		$this->load->model('Carousel');
		$this->load->model('Atlet');
		$this->load->model('Pelatih');
		$this->load->model('Users','usr');
		$this->load->model('Organisasi','org');
		$this->load->model('Beritax','news');
		$this->load->library('session');
	}

	public function index()
	{
		/*
		$this->load->view('admin/header', array('title' => 'Admin'));
		$this->load->view('admin/login');
		$this->load->view('admin/footer');
		*/
		if ($this->tank_auth->is_logged_in()) {									// logged in
			redirect('/admin/admin_page');

		} elseif ($this->tank_auth->is_logged_in(FALSE)) {						// logged in, not activated
			redirect('/auth/send_again/');
		} else {
			redirect('/auth');
		}
	}

	public function admin_page(){
		$this->load->view('admin/header', array('title' => 'Admin'));
		$this->load->view('admin/navbar');
		$this->load->view('admin/index');
		$this->load->view('admin/footer');
	}

	// add controller

	public function addBerita(){
		$this->load->view('admin/header', array('title' => 'Admin'));
		$this->load->view('admin/navbar');
		$this->load->view('admin/addBerita');
		$this->load->view('admin/footer');
	}

	public function addCarousel(){
		$this->load->view('admin/header', array('title' => 'Admin'));
		$this->load->view('admin/navbar');
		$this->load->view('admin/addCarousel');
		$this->load->view('admin/footer');
	}

	public function addAtlet(){
		$this->load->view('admin/header', array('title' => 'Admin'));
		$this->load->view('admin/navbar');
		$this->load->view('admin/addAtlet');
		$this->load->view('admin/footer');
	}

	public function addWasit(){
		$this->load->view('admin/header', array('title' => 'Admin'));
		$this->load->view('admin/navbar');
		$this->load->view('admin/addWasit');
		$this->load->view('admin/footer');
	}

	public function addPelatih(){
		$this->load->view('admin/header', array('title' => 'Admin'));
		$this->load->view('admin/navbar');
		$this->load->view('admin/addPelatih');
		$this->load->view('admin/footer');
	}

	public function addOrganisasi(){
		$this->load->view('admin/header', array('title' => 'Admin'));
		$this->load->view('admin/navbar');
		$this->load->view('admin/addOrganisasi');
		$this->load->view('admin/footer');
	}

	//submit add controller

	public function submitWasit(){
		$nama=$this->input->post('nama');
		$cabang=$this->input->post('cabang');
		$sertifikat=$this->input->post('sertifikat');
		$keterangan=$this->input->post('keterangan');
		$this->wasit->insert_entry($nama,$cabang,$sertifikat,$keterangan);
		$this->session->set_flashdata('item','Data Berhasil Ditambahkan');
		redirect('admin/addWasit');
	}

	public function submitAtlet(){
		$nama=$this->input->post('nama');
		$cabang=$this->input->post('cabang');
		$sertifikat=$this->input->post('sertifikat');
		$keterangan=$this->input->post('keterangan');
		$this->atlet->insert_entry($nama,$cabang,$sertifikat,$keterangan);
		$this->session->set_flashdata('item','Data Berhasil Ditambahkan');
		redirect('admin/addAtlet');
	}

	public function submitPelatih(){
		$nama=$this->input->post('nama');
		$cabang=$this->input->post('cabang');
		$sertifikat=$this->input->post('sertifikat');
		$keterangan=$this->input->post('keterangan');
		$this->pelatih->insert_entry($nama,$cabang,$sertifikat,$keterangan);
		$this->session->set_flashdata('item','Data Berhasil Ditambahkan');
		redirect('admin/addPelatih');
	}

	public function submitCarousel(){
		$judul=$this->input->post('judul');
		$subjudul=$this->input->post('subjudul');
		$newID=$this->Carousel->getLastestIndex()+1;

	 	$config['upload_path']   = './public/img/uploads/carousel/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size']      = 20000;
        $config['max_width']     = 1200;
        $config['max_height']    = 650;
        $config['overwrite'] = true;
 		$this->load->library('upload', $config);
        $this->upload->overwrite = true;

         $gambar1=$_FILES['userfile']['name'];
        
         $type1= pathinfo($gambar1, PATHINFO_EXTENSION);
      

        $gambar1=$newName1.".".$type1;
       
    
			$this->Carousel->insert_entry($judul,$subjudul,$gambar1);
			//update
			$id=$this->Carousel->getLastestIndex();
     			$newName1= $id."_".$judul."_1";
		 		$config['file_name'] = $newName1;
		 		$this->upload->initialize($config);
				
				
				if($this->upload->do_upload('userfile')){
						$status1="Berhasil";
				}else{
					$status1="Gagal Upload Gambar Karena Masalah Ukuran!";
				}
           if($status1==="Berhasil"){
           	  $gambar1=$newName1.".".$type1;
        		$this->Carousel->update_entry($judul,$subjudul,$gambar1,$id);
        		$status="Berhasil";
       		 }else{
       		 	$status="Gagal Upload Gambar Karena Masalah Ukuran!";
       		 }

	
		$this->session->set_flashdata('item',$status);
		redirect('admin/addCarousel');
	}


	public function submitOrganisasi(){
		$nama=$this->input->post('nama');
		$alamat=$this->input->post('alamat');
		$kelurahan=$this->input->post('kelurahan');
		$jumlah=$this->input->post('anggota');
		$kegiatan=$this->input->post('kegiatan');
		$tahun=$this->input->post('tahun');
		$keterangan=$this->input->post('keterangan');
		$this->org->insert_entry($nama,$alamat,$kelurahan,$jumlah,$kegiatan,$tahun,$keterangan);
		$this->session->set_flashdata('item','Data Berhasil Ditambahkan');
		redirect('admin/addOrganisasi');
	}

	public function submitBerita(){
		$namaid=$this->input->post('namaId');
		$judul=$this->input->post('judul');
		$isi=$this->input->post('isi');
		$newID=$this->news->getLastestIndex()+1;

		 $config['upload_path']   = './public/img/uploads/berita/';
         $config['allowed_types'] = 'gif|jpg|png';
         $config['max_size']      = 20000;
         $config['overwrite'] = true;
         $this->load->library('upload', $config);
         $this->upload->overwrite = true;

         $gambar1=$_FILES['userfile']['name'];
         $gambar2=$_FILES['userfile2']['name'];
         $type1= pathinfo($gambar1, PATHINFO_EXTENSION);
         $type2= pathinfo($gambar2, PATHINFO_EXTENSION);

        $gambar1=$newName1.".".$type1;
        $gambar2=$newName2.".".$type2;


 	
 			$this->news->insert_entry($namaid,$judul,$isi,$gambar1,$gambar2);
 	
 		

 		//---update
 		$id=$this->news->getLastestIndex();
 				$newName1= $id."_".$judul."_1";
		 		$config['file_name'] = $newName1;
		 		$this->upload->initialize($config);
				
				
				if($this->upload->do_upload('userfile')){
						$status1="Berhasil";
				}else{
					$status1="Gagal Upload Gambar Karena Masalah Ukuran!";
				}
//-------------------------------------------------------------------------------------
				$newName2= $id."_".$judul."_2";
			    $config['file_name'] = $newName2;
		 		$this->upload->initialize($config);
	
			   	
				if($this->upload->do_upload('userfile2')){
						$status2="Berhasil";
				}else{
					$status2="Gagal Upload Gambar Karena Masalah Ukuran!";
				}

  		$gambar1=$newName1.".".$type1;
        $gambar2=$newName2.".".$type2;

         if($status1==="Berhasil" and $status2==="Berhasil"){
        	 $this->news->update_entry_normal($namaid,$judul,$isi,$gambar1,$gambar2,$id);
        	 $status="Berhasil!";
        }else{
        	$status="Gagal Upload Gambar Karena Masalah Ukuran!";
        }



$this->session->set_flashdata('item',$status);



		redirect('admin/addBerita');
	}

	//update controller
	public function updatePelatih(){
		$this->load->view('admin/header', array('title' => 'Admin'));
		$this->load->view('admin/navbar');
		$this->load->view('admin/updatePelatih', array('data' => $this->pelatih->get_all() ));
		$this->load->view('admin/footer');
	}

	public function updateAtlet(){
		$this->load->view('admin/header', array('title' => 'Admin'));
		$this->load->view('admin/navbar');
		$this->load->view('admin/updateAtlet', array('data' => $this->atlet->get_all() ));
		$this->load->view('admin/footer');
	}

	public function updateWasit(){
		$this->load->view('admin/header', array('title' => 'Admin'));
		$this->load->view('admin/navbar');
		$this->load->view('admin/updateWasit', array('data' => $this->wasit->get_all() ));
		$this->load->view('admin/footer');
	}

	public function updateOrganisasi(){
		$this->load->view('admin/header', array('title' => 'Admin'));
		$this->load->view('admin/navbar');
		$this->load->view('admin/updateOrganisasi', array('data' => $this->org->get_all() ));
		$this->load->view('admin/footer');
	}

	public function updateBerita(){
		$this->load->view('admin/header', array('title' => 'Admin'));
		$this->load->view('admin/navbar');
		$this->load->view('admin/updateBerita', array('data' => $this->news->get_all() ));
		$this->load->view('admin/footer');
	}

	public function updateCarousel(){
		$this->load->view('admin/header', array('title' => 'Admin'));
		$this->load->view('admin/navbar');
		$this->load->view('admin/updateCarousel', array('data' => $this->Carousel->get_all() ));
		$this->load->view('admin/footer');
	}


	//form Update controller

	public function formUpdateOrganisasi(){
		$this->load->view('admin/header', array('title' => 'Admin'));
		$this->load->view('admin/navbar');
		$this->load->view('admin/formUpdateOrganisasi');
		$this->load->view('admin/footer');
	}

	public function formUpdatePelatih(){
		$this->load->view('admin/header', array('title' => 'Admin'));
		$this->load->view('admin/navbar');
		$this->load->view('admin/formUpdatePelatih');
		$this->load->view('admin/footer');
	}

	public function formUpdateCarousel(){
		$this->load->view('admin/header', array('title' => 'Admin'));
		$this->load->view('admin/navbar');
		$this->load->view('admin/formUpdateCarousel');
		$this->load->view('admin/footer');
	}

	public function formUpdateWasit(){
		$this->load->view('admin/header', array('title' => 'Admin'));
		$this->load->view('admin/navbar');
		$this->load->view('admin/formUpdateWasit');
		$this->load->view('admin/footer');
	}

	public function formUpdateAtlet(){
		$this->load->view('admin/header', array('title' => 'Admin'));
		$this->load->view('admin/navbar');
		$this->load->view('admin/formUpdateAtlet');
		$this->load->view('admin/footer');
	}

	public function formUpdateBerita(){
		$this->load->view('admin/header', array('title' => 'Admin'));
		$this->load->view('admin/navbar');
		$this->load->view('admin/formUpdateBerita');
		$this->load->view('admin/footer');
	}


	//do update controller
	public function resubmitAtlet(){
		$nama=$this->input->post('nama');
		$cabang=$this->input->post('cabang');
		$sertifikat=$this->input->post('sertifikat');
		$keterangan=$this->input->post('keterangan');
		$id=$this->input->post('id');

		$this->atlet->update_entry($nama,$cabang,$sertifikat,$keterangan,$id);
		$this->session->set_flashdata('item','Data '.$nama.'  Berhasil Diubah');
		redirect('admin/updateAtlet');
	}


	public function resubmitWasit(){
		$nama=$this->input->post('nama');
		$cabang=$this->input->post('cabang');
		$sertifikat=$this->input->post('sertifikat');
		$keterangan=$this->input->post('keterangan');
		$id=$this->input->post('id');

		$this->wasit->update_entry($nama,$cabang,$sertifikat,$keterangan,$id);
		$this->session->set_flashdata('item','Data '.$nama.'  Berhasil Diubah');
		redirect('admin/updateWasit');
	}


	public function resubmitPelatih(){
		$nama=$this->input->post('nama');
		$cabang=$this->input->post('cabang');
		$sertifikat=$this->input->post('sertifikat');
		$keterangan=$this->input->post('keterangan');
		$id=$this->input->post('id');

		$this->pelatih->update_entry($nama,$cabang,$sertifikat,$keterangan,$id);
		$this->session->set_flashdata('item','Data '.$nama.'  Berhasil Diubah');
		redirect('admin/updatePelatih');
	}

	public function resubmitOrganisasi(){
		$nama=$this->input->post('nama');
		$alamat=$this->input->post('alamat');
		$kelurahan=$this->input->post('kelurahan');
		$jumlah=$this->input->post('anggota');
		$kegiatan=$this->input->post('kegiatan');
		$tahun=$this->input->post('tahun');
		$keterangan=$this->input->post('keterangan');
		$id=$this->input->post('id');
		$this->org->update_entry($nama,$alamat,$kelurahan,$jumlah,$kegiatan,$tahun,$keterangan,$id);
		$this->session->set_flashdata('item','Data '.$nama.'  Berhasil Diubah');
		redirect('admin/updateOrganisasi');
	}

	public function resubmitBerita(){
		$id=$this->input->post('id');
		$namaid=$this->input->post('namaId');
		$judul=$this->input->post('judul');
		$isi=$this->input->post('isi');
		 $config['upload_path']   = './public/img/uploads/berita/';
         $config['allowed_types'] = 'gif|jpg|png';
         $config['max_size']      = 20000;
         $config['overwrite'] = true;
         $this->load->library('upload', $config);
         $this->upload->overwrite = true;

       	 $gambar1=$_FILES['userfile']['name'];
         $gambar2=$_FILES['userfile2']['name'];
         $type1 = pathinfo($gambar1, PATHINFO_EXTENSION);
         $type2= pathinfo($gambar2, PATHINFO_EXTENSION);

         if(!empty($gambar1) and !empty($gambar2)){
         		$newName1= $id."_".$judul."_1";
		 		$config['file_name'] = $newName1;
		 		$this->upload->initialize($config);
				
				
				if($this->upload->do_upload('userfile')){
						$status1="Berhasil";
				}else{
					$status1="Data Gambar Tidak Sesuai Ketentuan";
				}
//-------------------------------------------------------------------------------------
				$newName2= $id."_".$judul."_2";
			    $config['file_name'] = $newName2;
		 		$this->upload->initialize($config);
	
			   	
				if($this->upload->do_upload('userfile2')){
						$status2="Berhasil";
				}else{
					$status2="Data Gambar Tidak Sesuai Ketentuan";
				}

  		$gambar1=$newName1.".".$type1;
        $gambar2=$newName2.".".$type2;

         if($status1==="Berhasil" and $status2==="Berhasil"){
        	 $this->news->update_entry_normal($namaid,$judul,$isi,$gambar1,$gambar2,$id);
        }

       

         }else{
         	if(empty($gambar1) and empty($gambar2)){
         		$status3="Berhasil";
			    $this->news->update_entry_data($namaid,$judul,$isi,$id);
         	}else
         	if(empty($gambar2)){
         		$newName1= $id."_".$judul."_1";
		 		$config['file_name'] = $newName1;
		 		$this->upload->initialize($config);
				
				
				if($this->upload->do_upload('userfile')){
						$status3="Berhasil";
				}else{
					$status3="Data Gambar Tidak Sesuai Ketentuan";
				}
			   $gambar1=$newName1.".".$type1;

			   $this->news->update_entry_gambar1($namaid,$judul,$isi,$gambar1,$id);
		 		
         	}
         	else if(empty($gambar1)){
         		$newName2= $id."_".$judul."_2";
		 		$config['file_name'] = $newName2;
		 		$this->upload->initialize($config);
				
				
				if($this->upload->do_upload('userfile2')){
						$status3="Berhasil";
				}else{
					$status3="Data Gambar Tidak Sesuai Ketentuan";
				}
			   $gambar2=$newName2.".".$type2;
			    $this->news->update_entry_gambar2($namaid,$judul,$isi,$gambar2,$id);
		 		
         	}
         }
      

        if($status1==="Berhasil" and $status2==="Berhasil") {
        	$status="Berhasil";
        }else if($status3==="Berhasil"){
        	$status="Berhasil";
        }
        else{
        	$status="Gagal Upload Gambar Karena Masalah Ukuran!";
        }
        


 		$this->session->set_flashdata('item',$status);

		redirect('admin/updateBerita');
	}

	public function resubmitCarousel(){
		$id=$this->input->post('id');
		$judul=$this->input->post('judul');
		$subjudul=$this->input->post('subjudul');
		 $config['upload_path']   = './public/img/uploads/carousel/';
         $config['allowed_types'] = 'gif|jpg|png';
         $config['max_size']      = 20000;
         $config['max_width']     = 1200;
         $config['max_height']    = 650;
         $config['overwrite'] = true;
         $this->load->library('upload', $config);
         $this->upload->overwrite = true;
         $gambar1=$_FILES['userfile']['name'];
           $type1 = pathinfo($gambar1, PATHINFO_EXTENSION);
         if(!empty($gambar1)){
         		$newName1= $id."_".$judul."_1";
		 		$config['file_name'] = $newName1;
		 		$this->upload->initialize($config);
				
				
				if($this->upload->do_upload('userfile')){
						$status1="Berhasil";
				}else{
					$status1="Data Gambar Tidak Sesuai Ketentuan";
				}
           if($status1==="Berhasil"){
           	  $gambar1=$newName1.".".$type1;
        		$this->Carousel->update_entry($judul,$subjudul,$gambar1,$id);
       		 }
				
		 	
         }else{

			    	$status2="Berhasil";
			    	$this->Carousel->update_entry_gambar1($judul,$subjudul,$id);



         }
         if($status1==="Berhasil" or $status2==="Berhasil"){
         	$status="Berhasil";
         }else{
         	$status="Data Gambar Tidak Sesuai Ketentuan";
         }

 		$this->session->set_flashdata('item',$status);

		redirect('admin/updateCarousel');
	}

	//delete controller

	public function deleteBerita(){
		$this->load->view('admin/header', array('title' => 'Admin'));
		$this->load->view('admin/navbar');
		$this->load->view('admin/deleteBerita', array('data' => $this->news->get_all() ));
		$this->load->view('admin/footer');
	}

	public function deleteCarousel(){
		$this->load->view('admin/header', array('title' => 'Admin'));
		$this->load->view('admin/navbar');
		$this->load->view('admin/deleteCarousel', array('data' => $this->Carousel->get_all() ));
		$this->load->view('admin/footer');
	}

	public function deleteAtlet(){
		$this->load->view('admin/header', array('title' => 'Admin'));
		$this->load->view('admin/navbar');
		$this->load->view('admin/deleteAtlet', array('data' => $this->atlet->get_all() ));
		$this->load->view('admin/footer');
	}

	public function deleteWasit(){
		$this->load->view('admin/header', array('title' => 'Admin'));
		$this->load->view('admin/navbar');
		$this->load->view('admin/deleteWasit', array('data' => $this->wasit->get_all() ));
		$this->load->view('admin/footer');
	}

	public function deletePelatih(){
		$this->load->view('admin/header', array('title' => 'Admin'));
		$this->load->view('admin/navbar');
		$this->load->view('admin/deletePelatih', array('data' => $this->pelatih->get_all() ));
		$this->load->view('admin/footer');
	}


	public function deleteOrganisasi(){
		$this->load->view('admin/header', array('title' => 'Admin'));
		$this->load->view('admin/navbar');
		$this->load->view('admin/deleteOrganisasi', array('data' => $this->org->get_all() ));
		$this->load->view('admin/footer');
	}

	//submit delete


	public function submitDeleteAtlet(){
		$id=$this->input->post('id');
		$this->atlet->delete_entry($id);
		$this->session->set_flashdata('item','Data Berhasil Dihapus');
		redirect('admin/deleteAtlet');
	}

	public function submitDeleteWasit(){
		$id=$this->input->post('id');
		$this->wasit->delete_entry($id);
		$this->session->set_flashdata('item','Data Berhasil Dihapus');
		redirect('admin/deleteWasit');
	}

	public function submitDeleteCarousel(){
		$id=$this->input->post('id');
		$response=$this->Carousel->delete_entry($id);
		$this->session->set_flashdata('item',$response);
		redirect('admin/deleteCarousel');
	}

	public function submitDeletePelatih(){
		$id=$this->input->post('id');
		$this->pelatih->delete_entry($id);
		$this->session->set_flashdata('item','Data Berhasil Dihapus');
		redirect('admin/deletePelatih');
	}

	public function submitDeleteBerita(){
		$id=$this->input->post('id');
		$this->news->delete_entry($id);
		$this->session->set_flashdata('item','Data Berhasil Dihapus');
		redirect('admin/deleteBerita');
	}

	public function submitDeleteOrganisasi(){
		$id=$this->input->post('id');
		$this->org->delete_entry($id);
		$this->session->set_flashdata('item','Data Berhasil Dihapus');
		redirect('admin/deleteOrganisasi');
	}

	//update password controller
	public function settings(){
		/*
		$this->load->view('admin/header', array('title' => 'Admin'));
		$this->load->view('admin/navbar');
		$this->load->view('admin/changePassword');
		$this->load->view('admin/footer');
		*/
		redirect('auth/change_password');
	}

	//do update password controller
	public function updatepass(){

		$name=$this->input->post('name');
		$username=$this->input->post('username');
		$old=$this->input->post('old');
		$new=$this->input->post('new');
		$conf=$this->input->post('conf');
		if($new!==$conf){
			$response="Konfirmasi Password Tidak Sama";
		}else{
			$response=$this->usr->update($old,$new,$conf,$name,$username);
		}
		$this->session->set_flashdata('item',$response);
		redirect('admin/settings');
	}
}

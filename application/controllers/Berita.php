<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Berita extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/grafik
	 *	- or -
	 * 		http://example.com/index.php/grafik/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('berita/header', array('title' => 'Berita'));
		$this->load->view('common/navbar', setMenu('news'));
		$this->load->view('berita/index', array('pop' => $this->beritax->get_populer(), 'data' => $this->beritax->get_all() ));
		$this->load->view('berita/footer');
	}

	public function r()
	{
		$nama_id = $this->uri->segment(3, 0);

		$this->load->view('berita/header', array('title' => 'Berita'));
		$this->load->view('common/navbar', setMenu('berita'));
		$this->load->view('berita/contoh', array('berita' => $this->beritax->get_by_nama_id($nama_id) ));
		$this->load->view('berita/footer');
	}
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Grafik extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/grafik
	 *	- or -
	 * 		http://example.com/index.php/grafik/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('grafik/index');
	}

	public function atlet()
	{
		$this->load->view('grafik/atlet');
	}

	public function pelatih()
	{
		$this->load->view('grafik/pelatih');
	}

	public function wasit()
	{
		$this->load->view('grafik/wasit');
	}
}

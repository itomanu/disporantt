<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Login extends CI_Controller{
	public function __construct(){
		parent::__construct();
		$this->load->model('Users','users');
		$this->load->library('session');
	}
	public function index(){
		$username=$this->input->post('username');
		$password=$this->input->post('password');
		$response =  $this->users->get_by_data($username,$password);

			$this->session->set_userdata('username',$response);
			redirect('admin/admin_page');
	
	}

	public function dont_use_this(){
		$password="default";
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < 5; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    $username=$randomString;
	    $randomString2 = '';
	    for ($i = 0; $i < 8; $i++) {
	        $randomString2 .= $characters[rand(0, $charactersLength - 1)];
	    }
	    $name=$randomString2;
			
		$this->users->insert_entry($username,$name,$password);
	}

	public function logout(){
		$this->session->sess_destroy();
		redirect('admin/');
	}
}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tabel extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/grafik
	 *	- or -
	 * 		http://example.com/index.php/grafik/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('tabel/index');
	}

	public function atlet()
	{
		$this->load->view('tabel/header', array('title' => 'Atlet'));
		$this->load->view('common/navbar', setMenu('t_atlet'));

		$this->load->view('tabel/atlet', array('data' => $this->atlet->get_all() ));
		$this->load->view('tabel/footer');
	}

	public function pelatih()
	{
		$this->load->view('tabel/header', array('title' => 'Pelatih'));
		$this->load->view('common/navbar', setMenu('t_pelatih'));
		$this->load->view('tabel/pelatih', array('data' => $this->pelatih->get_all() ));
		$this->load->view('tabel/footer');
	}

	public function wasit()
	{
		$this->load->view('tabel/header', array('title' => 'Wasit'));
		$this->load->view('common/navbar', setMenu('t_wasit'));
		$this->load->view('tabel/wasit', array('data' => $this->wasit->get_all() ));
		$this->load->view('tabel/footer');
	}

	public function olahraga_pemuda()
	{
		$this->load->view('tabel/header', array('title' => 'Olahraga Pemuda'));
		$this->load->view('common/navbar', setMenu('t_olah_pemuda'));
		$this->load->view('tabel/olahraga-pemuda', array('data' => $this->organisasi->get_all() ));
		$this->load->view('tabel/footer');
	}
}

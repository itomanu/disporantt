<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if (!function_exists('setMenu')) {
  function setMenu($value='home') {
    $arrayName = array('home' => '',
      'news' => '',
      'g' => '',
      'g_atlet' => '',
      'g_wasit' => '',
      'g_pelatih' => '',
      't' => '',
      't_atlet' => '',
      't_wasit' => '',
      't_pelatih' => '',
      't_olah_pemuda' => '',
      't_organisasi' => '',
      't_sarana' => '',
      't_pengurus' => '',
      't_pengusaha' => '',
      't_klub' => '',
      'visi' => ''
    );

    $arrayName[$value] = 'active';
    if (substr($value,0,1) == 'g') {
      $arrayName['g'] = 'active';
    } elseif (substr($value,0,1) == 't') {
      $arrayName['t'] = 'active';
    }
    return $arrayName;
  }
}

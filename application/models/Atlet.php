<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Atlet extends CI_Model {
  private $tab_name;

  public $nama;
  public $cab_olahraga;
  public $sertifikat;
  public $keterangan;

  public function __construct()
  {
    // Call the CI_Model constructor
    parent::__construct();
    $this->tab_name = "atlet";
  }

  public function get_all()
  {
    $query = $this->db->get($this->tab_name);
    return $query->result();
  }

  public function get_by_id($id)
  {
    $this->db->where('id', $id);
    $query = $this->db->get($this->tab_name);
    return $query->row();
  }

  public function insert_entry($nama,$cab,$srtf,$ket)
  {
    $this->nama           = $nama;
    $this->cab_olahraga   = $cab;
    $this->sertifikat     = $srtf;
    $this->keterangan     = $ket;

    $this->db->insert($this->tab_name, $this);
  }

  public function update_entry($nama,$cab,$srtf,$ket,$id)
  {
    $this->nama           = $nama;
    $this->cab_olahraga   = $cab;
    $this->sertifikat     = $srtf;
    $this->keterangan     = $ket;

    $this->db->update($this->tab_name, $this, array('id' => $id));
  }

  public function delete_entry($id){
    $this->db->delete($this->tab_name, array('id' => $id)); 
  }
}

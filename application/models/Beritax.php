<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Beritax extends CI_Model {
  private $tab_name;
  public $id;
  public $nama_id;
  public $judul;
  public $isi;


  public function __construct()
  {
    // Call the CI_Model constructor
    parent::__construct();
    $this->tab_name = "berita";
  }

  public function get_all()
  {
    $this->db->order_by('waktu');
    $query = $this->db->get($this->tab_name);
    return $query->result();
  }

  public function getLastestIndex(){
    $this->db->select_max('id');
    $this->db->from("berita");
    $query= $this->db->get();
    return $query->row('id');
  }

  public function get_populer()
  {
    $this->db->limit(3);
    $this->db->order_by('jum_share');
    $query = $this->db->get($this->tab_name);
    return $query->result();
  }

  public function get_by_nama_id($nama_id)
  {
    $this->db->where('nama_id', $nama_id);
    $query = $this->db->get($this->tab_name);
    return $query->row();
  }

  public function get_by_id($id)
  {
    $this->db->where('id', $id);
    $query = $this->db->get($this->tab_name);
    return $query->row();
  }

  public function insert_entry($nama,$judul,$isi,$f1,$f2)
  {
    $this->nama_id    = $nama;
    $this->judul      = $judul;
    $this->isi        = $isi;
    $this->gambar_1   = $f1;
    $this->gambar_2   = $f2;

    $this->db->insert($this->tab_name, $this);
  }

  public function update_entry_normal($nama,$judul,$isi,$f1,$f2,$id)
  {
    
    $data = array(
               'nama_id' => $nama,
               'judul' => $judul,
               'isi' => $isi,
               'gambar_1' => $f1,
               'gambar_2' => $f2
            );


    $this->db->where('id', $id);
      $this->db->update($this->tab_name, $data); 
  }

  public function update_entry_gambar1($nama,$judul,$isi,$f1,$id){
    $data = array(
               'nama_id' => $nama,
               'judul' => $judul,
               'isi' => $isi,
               'gambar_1' => $f1
            );

      $this->db->where('id', $id);
      $this->db->update($this->tab_name, $data); 
  }

 public function update_entry_gambar2($nama,$judul,$isi,$f1,$id){
    $data = array(
               'nama_id' => $nama,
               'judul' => $judul,
               'isi' => $isi,
               'gambar_2' => $f1
            );

      $this->db->where('id', $id);
      $this->db->update($this->tab_name, $data); 
  }

   public function update_entry_data($nama,$judul,$isi,$id){
    $data = array(
               'nama_id' => $nama,
               'judul' => $judul,
               'isi' => $isi
            );

      $this->db->where('id', $id);
      $this->db->update($this->tab_name, $data); 
  }

   public function delete_entry($id){
    $this->db->delete($this->tab_name, array('id' => $id)); 
  }
}

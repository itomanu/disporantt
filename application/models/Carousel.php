<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Carousel extends CI_Model {
  private $tab_name;

  public $title;
  public $subtitle;
  public $img;

  public function __construct()
  {
    // Call the CI_Model constructor
    parent::__construct();
    $this->tab_name = "carousels";
  }

  public function get_all()
  {
    $query = $this->db->get($this->tab_name);
    return $query->result();
  }

  public function get_by_id($id)
  {
    $this->db->where('id', $id);
    $query = $this->db->get($this->tab_name);
    return $query->row();
  }

  public function getLastestIndex(){
    $this->db->select_max('id');
    $this->db->from("carousels");
    $query= $this->db->get();
    return $query->row('id');
  }

  public function countRow(){
     $query = $this->db->get($this->tab_name);
     return $rowcount = $query->num_rows();
  }

  public function insert_entry($title,$subtitle,$img)
  {
    $this->title      = $title;
    $this->subtitle   = $subtitle;
    $this->img        = $img;


    $this->db->insert($this->tab_name, $this);
  }

  public function update_entry($title,$subtitle,$img,$id)
  {
   $data = array(
               'title' => $title,
               'subtitle' => $subtitle,
               'img' =>$img
            );
   
      $this->db->where('id', $id);
      $this->db->update($this->tab_name, $data); 
  }

 public function update_entry_gambar1($judul,$subjudul,$id){
    $data = array(
               'title' => $judul,
               'subtitle' => $subjudul
            );

      $this->db->where('id', $id);
      $this->db->update($this->tab_name, $data); 
  }

  public function delete_entry($id){
    if( $this->countRow() > 1){
      $this->db->delete($this->tab_name, array('id' => $id)); 
      $response="Data Berhasil Dihapus";
    }else{
      $response="Tidak Bisa Dihapus, Minimal 1 Carousel Tersisa";
    }
    return $response;
  }
}

<!-- Marketing messaging and featurettes
================================================== -->
<!-- Wrap the rest of the page in another container to center all the content. -->


<div class="container marketing" style="margin-top: 80px;">

  <div class="row">
    <h2 style="text-align: center;margin-bottom: 25px;">Tambah Data Organisasi</h2>
    
        <?php if($this->session->flashdata('item')!=""){
        echo "<div class='alert-success'>";
        echo "<p>".$this->session->flashdata('item')."</p>";
        echo "</div>";
        }
      ?>
    <div class="row" style="float: center; margin-left: 25%;" >
      <form style="width: 70%;"  action="<?php echo base_url('admin/submitOrganisasi');?>" method="POST">
        Nama Organisasi: <input type="text" name="nama" id="nama" placeholder="nama" required></input>
        Alamat: <input type="text" name="alamat" id="alamat" placeholder="alamat" required></input>
        Kelurahan: <input type="text" name="kelurahan" id="kelurahan" placeholder="kelurahan" required></input>
        Jumlah Anggota:   <input type="number" min=1 name="anggota" id="anggota" placeholder="jumlah anggota" required></input>
        Kegiatan:  <input type="text" name="kegiatan" id="kegiatan" placeholder="kegiatan"></input>
        Tahun Berdiri:  <input type="number" min="1900" name="tahun" id="tahun" placeholder="tahun" required></input>
        Keterangan: <textarea name="keterangan" id="keterangan" rows="4" cols="70" wrap="hard"></textarea> 
        <input type="submit" value="Submit" id="submit"></input>
      </form>
    <hr >
    </div>

  </div>

  <!-- START THE FEATURETTES -->

  
  <!-- /END THE FEATURETTES -->

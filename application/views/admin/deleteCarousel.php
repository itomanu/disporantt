<!-- Tabels
================================================== -->
<!-- Wrap the rest of the page in another container to center all the content. -->

<div class="container container-tabel">


<!-- START THE TABLE -->

<h1>Hapus Data Carousel</h1>
<?php if($this->session->flashdata('item')!=""){
        echo "<div class='alert-danger'>";
        echo "<p>".$this->session->flashdata('item')."</p>";
        echo "</div>";
        }
      ?>
<table id="tabel" class="table table-bordered table-hover">
    <thead>
    <tr>
      <th>No</th>
      <th>Judul</th>
      <th>Sub-Judul</th>
      <th>Gambar</th>
      <th>Aksi</th>
    </tr>
    </thead>
    <tbody>
      <?php
      $i = 1;
      foreach ($data as $row) {
        echo "
          <tr>";
          ?> <form action="<?php echo base_url('admin/submitDeleteCarousel');?>" onsubmit="return confirm('Apakah Anda Yakin Untuk Menghapus?');" method="POST"><?php echo "
            <td>$i</td>
            <input type='hidden' name='id' id='id' value='$row->id'/>
            <td>$row->title</td>
            <td>$row->subtitle</td>
            <td><img class='img-view' src='".base_url('public/img/'.$row->img)."'/></td>
            <td><input type='submit' value='Hapus'/></td>
            </form>
          </tr>";
        $i++;
      } ?>
    </tbody>
  </table>

<hr class="featurette-divider">

<!-- /END THE FEATURETTES -->

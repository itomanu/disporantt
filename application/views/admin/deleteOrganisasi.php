<!-- Tabels
================================================== -->
<!-- Wrap the rest of the page in another container to center all the content. -->

<div class="container container-tabel">


<!-- START THE TABLE -->

<h1>Hapus Data Organisasi</h1>
<?php if($this->session->flashdata('item')!=""){
        echo "<div class='alert-danger'>";
        echo "<p>".$this->session->flashdata('item')."</p>";
        echo "</div>";
        }
      ?>
<table id="tabel" class="table table-bordered table-hover">
    <thead>
    <tr>
      <th>No</th>
      <th>Nama</th>
      <th>Alamat</th>
      <th>Kelurahan</th>
      <th>Jumlah Anggota</th>
      <th>Kegiatan</th>
      <th>Tahun</th>
      <th>Keterangan</th>
      <th>Aksi</th>
    </tr>
    </thead>
    <tbody>
      <?php
      $i = 1;
      foreach ($data as $row) {
        echo "
          <tr>";
          ?> <form action="<?php echo base_url('admin/submitDeleteOrganisasi');?>" onsubmit="return confirm('Apakah Anda Yakin Untuk Menghapus?');" method="POST"><?php echo "
            <td>$i</td>
            <input type='hidden' name='id' id='id' value='$row->id'/>
            <td>$row->nama</td>
            <td>$row->alamat</td>
            <td>$row->kelurahan</td>
            <td>$row->jum_anggota</td>
            <td>$row->kegiatan</td>
            <td>$row->tahun</td>
            <td>$row->keterangan</td>
            <td><input type='submit' value='Hapus ...'/></td>
            </form>
          </tr>";
        $i++;
      } ?>
    </tbody>
  </table>

<hr class="featurette-divider">

<!-- /END THE FEATURETTES -->

<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="<?php echo base_url('favicon.ico')?>">

    <title><?php echo $title ?> - Dinas Pemudaan & Olahraga Kota Kupang </title>
      <!-- Font Awesome -->
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
      <!-- Ionicons -->
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="<?php echo base_url('public/assets/css/ie10-viewport-bug-workaround.css')?>" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="<?php echo base_url('public/assets/js/ie-emulation-modes-warning.js')?>"></script>
    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
    <script>tinymce.init({ selector:'textarea' });</script>



    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" href="<?php echo base_url('public/dist/css/AdminLTE.min.css')?>">

    <!-- Custom styles for this template -->
    <link href="<?php echo base_url('public/css/carousel.css')?>" rel="stylesheet">
    <style>
      * {
          box-sizing:border-box;
          padding:0;
          margin:0;
        }

        .alert-success,.alert-danger,.alert-warning{
          width:320px;
          margin:30px auto;
          height:25px;
          position:relative;
          padding:0px;
          border-radius:0.6em;


       /* -moz-animation: cssAnimation 0s ease-in 5s forwards;
         Firefox
        -webkit-animation: cssAnimation 0s ease-in 5s forwards;
        /* Safari and Chrome
         -o-animation: cssAnimation 0s ease-in 5s forwards;
        /* Opera
        animation: cssAnimation 0s ease-in 5s forwards;
        -webkit-animation-fill-mode: forwards;
         animation-fill-mode: forwards;*/

        }

        @keyframes cssAnimation {
            to {
                width:0;
                height:0;
                overflow:hidden;
            }
        }

        @-webkit-keyframes cssAnimation {
            to {
                width:0;
                height:0;
                visibility:hidden;
            }
        }

        .alert-success, .alert-danger, .alert-warning p{
            text-align: center;
        }



        #main {
          width:320px;
          margin:30px auto;
          height:230px;
          position:relative;
          padding:6px;
          border:1px solid rgba(0,0,0,0.3);
          border-radius:0.6em;
          background:#f6f4f1;
          box-shadow: 0 0 5px rgba(0,0,0,0.5);

        }


        #main:after {
            clear:both;
          content:"";
          display:table;
        }

        #inset {
          border:4px solid rgba(255,0,0,0.2);
          border-radius:0.5em;
          float:left;
          width:100%;
          padding:3px 10px 6px 40px;
          height:100%;

        }



        input[type="text"],
        input[type="password"],input[type="number"]{
             border-radius:0.3em;
          border:1px solid #91897e;
          box-shadow:inset 0 0 4px rgba(25,0,0,0.4);
          padding:7px;
          margin:5px 0;
          transition:all 0.3s ease-out;
          width:100%;
          height:34px;
        }
        textarea{
          border-radius:0.3em;
          border:1px solid #91897e;
          box-shadow:inset 0 0 4px rgba(25,0,0,0.4);
          padding:7px;
          margin:5px 0;
          transition:all 0.3s ease-out;
          width:100%;
          height:80px;
        }
        input[type="password"]{


        }

        input[type="submit"]{
            float:left;
          width:46%;
          margin-left:4%;
          margin-top:5px;
          height:34px;
          border:1px solid #91897e;
          border-radius:0.3em;
          padding:7px;
          transition:all 0.3s ease-out;
          box-shadow:
            inset 0 1px 0 rgba(255,255,255,0.5),
            inset 0 20px 20px rgba(255,255,255,0.1);
          cursor:pointer;
          background:rgba(250,0,0,0.2);
          font-weight:bold;
          color:#aa6b40;
          text-shadow:0 1px 0 rgba(255,255,255,0.5);
          font-family:verdana;
        }

        ::-webkit-input-placeholder {
           font-family:verdana;
           text-transform:uppercase;
           font-size:10px;
           line-height:16px;
           letter-spacing:1px;
        }


        input[type="text"]:hover,
        input[type="password"]:hover{
            box-shadow:none;
        }

        input[type="submit"]:hover {
             background:rgba(250,0,0,0.3);
        }

        input[type="submit"]#submit:hover {
             background:rgba(250,255,,0.3);
        }

        input[type="submit"]#submit{
            float:left;
          width:100%;
          margin-left:0;
          margin-top:15px;
          height:34px;
          border:1px solid #91897e;
          border-radius:0.3em;
          padding:7px;
          transition:all 0.3s ease-out;
          box-shadow:
            inset 0 1px 0 rgba(255,255,255,0.5),
            inset 0 20px 20px rgba(255,255,255,0.1);
          cursor:pointer;
          background:#93e6ff;
          font-weight:bold;
          color:black;
          text-shadow:0 1px 0 rgba(255,255,255,0.5);
          font-family:verdana;
        }

        .dropdown-submenu {
            position:relative;
        }

        .dropdown-submenu>.dropdown-menu {
            top:0;
            left:100%;
            margin-top:-6px;
            margin-left:-1px;
            -webkit-border-radius:0 6px 6px 6px;
            -moz-border-radius:0 6px 6px 6px;
            border-radius:0 6px 6px 6px;
        }

        .dropdown-submenu:hover>.dropdown-menu {
            display:block;
        }


        .dropdown-submenu>a:after {
            display:block;
            content:" ";
            float:right;
            width:0;
            height:0;
            border-color:transparent;
            border-style:solid;
            border-width:5px 0 5px 5px;
            border-left-color:#cccccc;
            margin-top:5px;
            margin-right:-10px;
        }

        .dropdown-submenu:hover>a:after {
            border-left-color:#ffffff;
        }

        .dropdown-submenu.pull-left {
            float:none;
        }

        .dropdown-submenu.pull-left>.dropdown-menu {
            left:-100%;
            margin-left:10px;
            -webkit-border-radius:6px 0 6px 6px;
            -moz-border-radius:6px 0 6px 6px;
            border-radius:6px 0 6px 6px;
        }

        .img-view {
          height: 100px;
        }

        table {border-collapse:collapse; table-layout:fixed; }
        table td {border:solid 1px #fab;  word-wrap:break-word;}

    </style>
  </head>
  <body>

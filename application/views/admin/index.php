<!-- Marketing messaging and featurettes
================================================== -->
<!-- Wrap the rest of the page in another container to center all the content. -->


<div class="container marketing" style="margin-top: 80px;">
  <div class="jumbotron">
    <h1>Selamat datang!</h1>
    <p>Silahkan pilih menu yang tersedia pada navigasi di atas.</p>
    <p>Untuk keamanan segera ubah password default, jika sudah abaikan pesan ini.</p>
    <p>Menu untuk mengubah password ada pada navigasi 'admin' di bagian kanan atas.</p>
  </div>
</div>

<?php
if($this->session->userdata('username')==""){
    redirect('admin');
}
?>
  <div class="navbar-wrapper">
    <div class="container">

      <nav class="navbar navbar-default navbar-static-top">
        <div class="container">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
          </div>
          <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
             <li><a href="<?php echo base_url('admin/admin_page') ?>"><b>Admin Dispora Kota Kupang</b></a></li>
               <li class="dropdown ">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Informasi Web<span class="caret"></span></a>
                 <ul class="dropdown-menu">
                         <li class="menu-item dropdown dropdown-submenu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Berita</a>
                            <ul class="dropdown-menu">
                               <li><a href="<?php echo base_url('admin/berita') ?>">Semua Berita</a></li>
                               <li><a href="<?php echo base_url('admin/addBerita') ?>">Buat Berita</a></li>
                              <li><a href="<?php echo base_url('admin/updateBerita') ?>">Update Berita</a></li>
                              <li><a href="<?php echo base_url('admin/deleteBerita') ?>">Hapus Berita</a></li>
                            </ul>
                        </li>
                        <li class="menu-item dropdown dropdown-submenu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Halaman Depan</a>
                            <ul class="dropdown-menu">
                               <li><a href="<?php echo base_url('admin/carousel') ?>">Semua Data</a></li>
                               <li><a href="<?php echo base_url('admin/addCarousel') ?>">Tambah Data</a></li>
                                <li><a href="<?php echo base_url('admin/updateCarousel') ?>">Update Data </a></li>
                                <li><a href="<?php echo base_url('admin/deleteCarousel') ?>">Hapus Data </a></li>
                            </ul>
                        </li>

                  </ul>
              </li>

              <li class="dropdown ">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Personal<span class="caret"></span></a>
                 <ul class="dropdown-menu">
                         <li class="menu-item dropdown dropdown-submenu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Atlet</a>
                            <ul class="dropdown-menu">
                                <li><a href="<?php echo base_url('admin/atlet') ?>">Semua Atlet</a></li>
                                <li><a href="<?php echo base_url('admin/addAtlet') ?>">Tambah Data Atlet</a></li>
                                <li><a href="<?php echo base_url('admin/updateAtlet') ?>">Update Data Atlet</a></li>
                                <li><a href="<?php echo base_url('admin/deleteAtlet') ?>">Hapus Data Atlet</a></li>
                            </ul>
                        </li>
                        <li class="menu-item dropdown dropdown-submenu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Wasit</a>
                            <ul class="dropdown-menu">
                                <li><a href="<?php echo base_url('admin/wasit') ?>">Semua Wasit</a></li>
                                <li><a href="<?php echo base_url('admin/addWasit') ?>">Tambah Data Wasit</a></li>
                                <li><a href="<?php echo base_url('admin/updateWasit') ?>">Update Data Wasit</a></li>
                                <li><a href="<?php echo base_url('admin/deleteWasit') ?>">Hapus Data Wasit</a></li>
                            </ul>
                        </li>
                        <li class="menu-item dropdown dropdown-submenu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Pelatih</a>
                            <ul class="dropdown-menu">
                              <li><a href="<?php echo base_url('admin/pelatih') ?>">Semua Pelatih</a></li>
                              <li><a href="<?php echo base_url('admin/addPelatih') ?>">Tambah Data Pelatih</a></li>
                              <li><a href="<?php echo base_url('admin/updatePelatih') ?>">Update Data Pelatih</a></li>
                              <li><a href="<?php echo base_url('admin/deletePelatih') ?>">Hapus Data Pelatih</a></li>
                            </ul>
                        </li>
                  </ul>
              </li>
              <li class="dropdown ">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Administratif<span class="caret"></span></a>
                 <ul class="dropdown-menu">
                         <li class="menu-item dropdown dropdown-submenu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Sarana & Prasarana</a>
                            <ul class="dropdown-menu">
                                  <li><a href="<?php echo base_url('admin/admin_page') ?>">Semua Data </a></li>
                                  <li><a href="<?php echo base_url('admin/admin_page') ?>">Tambah Data </a></li>
                                  <li><a href="<?php echo base_url('admin/admin_page') ?>">Update Data </a></li>
                                  <li><a href="<?php echo base_url('admin/admin_page') ?>">Hapus Data </a></li>
                            </ul>
                        </li>
                        <li class="menu-item dropdown dropdown-submenu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Pengurus Cabang</a>
                            <ul class="dropdown-menu">
                               <li><a href="<?php echo base_url('admin/admin_page') ?>">Semua Data </a></li>
                               <li><a href="<?php echo base_url('admin/admin_page') ?>">Tambah Data </a></li>
                                <li><a href="<?php echo base_url('admin/admin_page') ?>">Update Data </a></li>
                                <li><a href="<?php echo base_url('admin/admin_page') ?>">Hapus Data </a></li>
                            </ul>
                        </li>
                        <li class="menu-item dropdown dropdown-submenu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Pemuda Pengusaha</a>
                            <ul class="dropdown-menu">
                               <li><a href="<?php echo base_url('admin/admin_page') ?>">Semua Data </a></li>
                               <li><a href="<?php echo base_url('admin/admin_page') ?>">Tambah Data </a></li>
                              <li><a href="<?php echo base_url('admin/admin_page') ?>">Update Data </a></li>
                              <li><a href="<?php echo base_url('admin/admin_page') ?>">Hapus Data </a></li>
                            </ul>
                        </li>
                  </ul>
              </li>
              <li class="dropdown ">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Organisasi<span class="caret"></span></a>
                 <ul class="dropdown-menu">
                         <li class="menu-item dropdown dropdown-submenu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Organisasi</a>
                            <ul class="dropdown-menu">
                                <li><a href="<?php echo base_url('admin/organisasi') ?>">Semua Organisasi</a></li>
                                <li><a href="<?php echo base_url('admin/addOrganisasi') ?>">Tambah Data Organisasi</a></li>
                                <li><a href="<?php echo base_url('admin/updateOrganisasi') ?>">Update Data Organisasi</a></li>
                                <li><a href="<?php echo base_url('admin/deleteOrganisasi') ?>">Hapus Data Organisasi</a></li>
                            </ul>
                        </li>
                        <li class="menu-item dropdown dropdown-submenu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Klub Cabang Olahraga</a>
                            <ul class="dropdown-menu">
                               <li><a href="<?php echo base_url('admin/admin_page') ?>">Semua Data </a></li>
                               <li><a href="<?php echo base_url('admin/admin_page') ?>">Tambah Data </a></li>
                                <li><a href="<?php echo base_url('admin/admin_page') ?>">Update Data </a></li>
                                <li><a href="<?php echo base_url('admin/admin_page') ?>">Hapus Data </a></li>
                            </ul>
                        </li>

                  </ul>
              </li>

              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $this->session->userdata('username');?><span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="<?php echo base_url('admin/settings') ?>">Update Password</a></li>
                  <li><a href="<?php echo base_url('login/logout') ?>">Logout</a></li>
                </ul>
              </li>
            </ul>
          </div>
        </div>
      </nav>

    </div>
  </div>

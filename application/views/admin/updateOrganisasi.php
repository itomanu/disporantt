<!-- Tabels
================================================== -->
<!-- Wrap the rest of the page in another container to center all the content. -->

<div class="container container-tabel">


<!-- START THE TABLE -->

<h1>Update Data Organisasi</h1>
<?php if($this->session->flashdata('item')!=""){
        echo "<div class='alert-success'>";
        echo "<p>".$this->session->flashdata('item')."</p>";
        echo "</div>";
        }
      ?>
<table id="tabel" class="table table-bordered table-hover">
    <thead>
    <tr>
      <th>No</th>
      <th>Nama</th>
      <th>Alamat</th>
      <th>Kelurahan</th>
      <th>Jumlah Anggota</th>
      <th>Kegiatan</th>
      <th>Tahun</th>
      <th>Keterangan</th>
      <th>Aksi</th>
    </tr>
    </thead>
    <tbody>
      <?php
      $i = 1;
      foreach ($data as $row) {
        echo "
          <tr>";
          ?> <form action="<?php echo base_url('admin/formUpdateOrganisasi');?>"  method="POST"><?php echo "
            <td>$i</td>
            <input type='hidden' name='id' id='id' value='$row->id'/>
            <td>$row->nama</td>
            <input type='hidden' name='nama' id='nama' value='$row->nama'/>
            <td>$row->alamat</td>
            <input type='hidden' name='alamat' id='alamat' value='$row->alamat'/>
            <td>$row->kelurahan</td>
            <input type='hidden' name='kelurahan' id='kelurahan' value='$row->kelurahan'/>
            <td>$row->jum_anggota</td>
            <input type='hidden' name='jum_anggota' id='jum_anggota' value='$row->jum_anggota'/>
            <td>$row->kegiatan</td>
            <input type='hidden' name='kegiatan' id='kegiatan' value='$row->kegiatan'/>
            <td>$row->tahun</td>
            <input type='hidden' name='tahun' id='tahun' value='$row->tahun'/>
            <td>$row->keterangan</td>
            <input type='hidden' name='keterangan' id='keterangan' value='$row->keterangan'/>
            <td><input type='submit' id='submit' value='Update'/></td>
            </form>
          </tr>";
        $i++;
      } ?>
    </tbody>
  </table>

<hr class="featurette-divider">

<!-- /END THE FEATURETTES -->

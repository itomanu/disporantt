<?php
$old_password = array(
	'name'	=> 'old_password',
	'id'	=> 'old_password',
	'value' => set_value('old_password'),
	'size' 	=> 30,
);
$new_password = array(
	'name'	=> 'new_password',
	'id'	=> 'new_password',
	'maxlength'	=> $this->config->item('password_max_length', 'tank_auth'),
	'size'	=> 30,
);
$confirm_new_password = array(
	'name'	=> 'confirm_new_password',
	'id'	=> 'confirm_new_password',
	'maxlength'	=> $this->config->item('password_max_length', 'tank_auth'),
	'size' 	=> 30,
);
?>
<!-- Marketing messaging and featurettes
================================================== -->
<!-- Wrap the rest of the page in another container to center all the content. -->


<div class="container marketing" style="margin-top: 80px;">

  <div class="row">
    <h2 style="text-align: center;margin-bottom: 25px;">Change Password</h2>
        <?php if(! is_null($this->session->flashdata('item'))){

        	if($this->session->flashdata('item')=='0'){
        		 echo "<div class='alert-success'>";
		        echo "<p>Password Berhasil Diganti</p>";
		        echo "</div>";
        	}else{
        		 echo "<div class='alert-danger'>";
		        echo "<p>Terjadi Kesalahan</p>";
		        echo "</div>";

        	}
       
        }
      ?>
    <div class="row" style="float: center; margin-left: 35%; width: 30%;" >
		<?php echo form_open($this->uri->uri_string()); ?>
		<p><?php echo form_label('Old Password', $old_password['id']); ?></p>
		<p style="color: red; margin-top: -4%;"> 
			<?php echo form_error($old_password['name']); ?><?php echo isset($errors[$old_password['name']])?$errors[$old_password['name']]:''; ?> 
		</p>
		<p style="margin-top: -4%;"><?php echo form_password($old_password); ?></p>
		
		<p>
		<?php echo form_label('New Password', $new_password['id']); ?>
		</p>
		<p style="color: red; margin-top: -4%;">
		<?php echo form_error($new_password['name']); ?><?php echo isset($errors[$new_password['name']])?$errors[$new_password['name']]:''; ?>
		</p>
		<p style="margin-top: -4%;">
		<?php echo form_password($new_password); ?>
		</p>

		<p>
			<?php echo form_label('Confirm New Password', $confirm_new_password['id']); ?>
		</p>
		<p style="color: red; margin-top: -4%;">
			<?php echo form_error($confirm_new_password['name']); ?><?php echo isset($errors[$confirm_new_password['name']])?$errors[$confirm_new_password['name']]:''; ?>
		</p>
		<p style="margin-top: -4%;" >
			<?php echo form_password($confirm_new_password); ?>
		</p>
		
		<?php echo form_submit('change', 'Change Password'); ?></p>
		<?php echo form_close(); ?>
    <hr >
    </div>

  </div>

  <!-- START THE FEATURETTES -->

  <hr class="featurette-divider">

  
  <!-- /END THE FEATURETTES -->

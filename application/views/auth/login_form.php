<?php
$login = array(
	'name'	=> 'login',
	'id'	=> 'login',
	'placeholder' => 'Your Email Address',
	'value' => set_value('login'),
	'maxlength'	=> 80,
	'size'	=> 30,
);
if ($login_by_username AND $login_by_email) {
	$login_label = 'Email or login';
} else if ($login_by_username) {
	$login_label = 'Login';
} else {
	$login_label = 'Email';
}
$password = array(
	'name'	=> 'password',
	'id'	=> 'password',
	'placeholder' => 'Your Password',
	'size'	=> 30,
);
$remember = array(
	'name'	=> 'remember',
	'id'	=> 'remember',
	'value'	=> 1,
	'checked'	=> set_value('remember'),
	'style' => 'margin:0;padding:0',
);
$captcha = array(
	'name'	=> 'captcha',
	'id'	=> 'captcha',
	'maxlength'	=> 8,
);
?>
<!-- Marketing messaging and featurettes
================================================== -->
<!-- Wrap the rest of the page in another container to center all the content. -->


<div class="container marketing" style="margin-top: 80px;">

  <div class="row">
    <h2 style="text-align: center;margin-bottom: 25px;">Login Admin Dispora Kupang</h2>

	<?php echo form_open($this->uri->uri_string()); ?>
 <div id="main">

        <div id="inset">

          <p>
            <?php echo form_label($login_label, $login['id']); ?>
          </p>
          <p style="color: red; margin-top: -7%;">
          <?php echo form_error($login['name']); ?>
          <?php echo isset($errors[$login['name']])?$errors[$login['name']]:''; ?>
          </p>
          <p style="margin-top: -7%;">
            <?php echo form_input($login); ?>
          </p>
          <p>
            <?php echo form_label('Password', $password['id']); ?>
          </p>
          <p style="color: red; margin-top: -7%;">
            <?php echo form_error($password['name']); ?><?php echo isset($errors[$password['name']])?$errors[$password['name']]:''; ?>
          </p>
          <p style="margin-top: -7%;">
            <?php echo form_password($password); ?>
          </p>
			<?php echo form_submit('submit', 'Sign in'); ?>
			<?php echo form_close(); ?>
			<p>
			<?php if ($this->config->item('allow_registration', 'tank_auth')) echo anchor('/auth/register/', 'Register'); ?>
			</p>
        </div>
    </div>



  </div>

  <!-- START THE FEATURETTES -->

  <hr class="featurette-divider">


  <!-- /END THE FEATURETTES -->

    <!-- Tabels
    ================================================== -->
    <!-- Wrap the rest of the page in another container to center all the content. -->

    <div class="container container-berita">


      <!-- START THE TABLE -->

      <h1><?php echo $berita->judul ?></h1>

      <div class="time-berita">
        <p><?php echo $berita->waktu ?></p>
      </div>

      <div class="content-berita">
        <img class="img-responsive center-block img-berita" src="<?php echo base_url('$berita->gambar_1') ?>" alt="<?php echo $berita->judul?> Gambar 1">
        <p>
          <?php echo $berita->isi ?>
        </p>
        <img class="img-responsive center-block img-berita" src="<?php echo base_url('$berita->gambar_2') ?>" alt="<?php echo $berita->judul?> Gambar 2">
      </div>

      <hr class="featurette-divider">

      <!-- /END THE FEATURETTES -->

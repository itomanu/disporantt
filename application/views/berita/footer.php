

      <!-- FOOTER -->
      <footer>
        <p class="pull-right"><a href="#">Kembali ke atas</a></p>
        <p>&copy; 2016 Dispora Kota Kupang. &middot;</p>
      </footer>

    </div><!-- /.container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="<?php echo base_url('public/js/bootstrap.min.js')?>"></script>
    <!-- Just to make our placeholder images work. Don't actually copy the next line! -->
    <script src="<?php echo base_url('public/assets/js/vendor/holder.min.js')?>"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="<?php echo base_url('public/assets/js/ie10-viewport-bug-workaround.js')?>"></script>
    <script src="<?php echo base_url('public/plugins/datatables/jquery.dataTables.min.js')?>"></script>
    <script src="<?php echo base_url('public/plugins/datatables/dataTables.bootstrap.min.js')?>"></script>

    <script>
      $(function () {
        $("#tabel").DataTable();
      });
    </script>

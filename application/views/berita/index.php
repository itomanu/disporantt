<!-- Marketing messaging and featurettes
================================================== -->
<!-- Wrap the rest of the page in another container to center all the content. -->


<div class="container marketing" style="margin-top: 80px;">

  <div class="row">
    <h2 style="text-align: center;margin-bottom: 25px;">BERITA POPULER</h2>
    <div class="row">
      <?php
      foreach ($pop as $row) {
        echo "
          <div class=\"col-md-4\">
            <img class=\"img-responsive center-block\" data-src=\"holder.js/400x200/auto\" alt=\"Generic placeholder image\">
            <h2>$row->judul</h2>
            <p>$row->isi</p>
            <a href=\"".base_url('berita/r/'.$row->nama_id)."\">Selengkapnya...</a>
          </div>";
      } ?>
    </div>

  </div>

  <!-- START THE FEATURETTES -->

  <hr class="featurette-divider">

  <?php
  foreach ($data as $row) {
    echo "
      <div class=\"row featurette\">
        <div class=\"col-md-7\">
          <h2 class=\"featurette-heading\">$row->judul</h2>
          <p class=\"lead\">$row->isi</p>
          <a href=\"".base_url('berita/r/'.$row->nama_id)."\">Selengkapnya...</a>
        </div>
        <div class=\"col-md-5\">
          <img class=\"featurette-image img-responsive center-block\" data-src=\"holder.js/500x500/auto\" alt=\"Generic placeholder image\">
        </div>
      </div>
      <hr class=\"featurette-divider\">

      ";
  } ?>
  
  <!-- /END THE FEATURETTES -->

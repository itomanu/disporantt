
  <div class="navbar-wrapper">
    <div class="container">

      <nav class="navbar navbar-default navbar-static-top">
        <div class="container">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
          </div>
          <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
              <li class="<?php echo $home ?>"><a href="<?php echo base_url() ?>">Beranda</a></li>
              <li class="<?php echo $news ?>"><a href="<?php echo base_url('berita') ?>">Berita</a></li>
              <li class="dropdown <?php echo $g ?>">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Grafik<span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li class="<?php echo $g_atlet ?>"><a href="<?php echo base_url('grafik/atlet') ?>">Atlet</a></li>
                  <li class="<?php echo $g_wasit ?>"><a href="<?php echo base_url('grafik/wasit') ?>">Wasit</a></li>
                  <li class="<?php echo $g_pelatih ?>"><a href="<?php echo base_url('grafik/pelatih') ?>">Pelatih</a></li>
                </ul>
              </li>
              <li class="dropdown <?php echo $t ?>">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Data Tabel<span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li class="<?php echo $t_atlet ?>"><a href="<?php echo base_url('tabel/atlet') ?>">Atlet</a></li>
                  <li class="<?php echo $t_wasit ?>"><a href="<?php echo base_url('tabel/wasit') ?>">Wasit</a></li>
                  <li class="<?php echo $t_pelatih ?>"><a href="<?php echo base_url('tabel/pelatih') ?>">Pelatih</a></li>
                  <li class="<?php echo $t_olah_pemuda ?>"><a href="<?php echo base_url('tabel/olahraga-pemuda') ?>">Olahraga Pemuda</a></li>
                  <li class="<?php echo $t_organisasi ?>"><a href="<?php echo base_url('tabel/organisasi') ?>">Biodata Organisasi</a></li>
                  <li class="<?php echo $t_sarana ?>"><a href="<?php echo base_url('tabel/sarana-prasarana') ?>">Sarana & Prasarana</a></li>
                  <li class="<?php echo $t_pengurus ?>"><a href="<?php echo base_url('tabel/pengurus') ?>">Pengurus Cabang Olahraga</a></li>
                  <li class="<?php echo $t_pengusaha ?>"><a href="<?php echo base_url('tabel/pengusaha') ?>">Pemuda Pengusaha</a></li>
                  <li class="<?php echo $t_klub ?>"><a href="<?php echo base_url('tabel/klub') ?>">Klub Cabang Olahraga</a></li>
                </ul>
              </li>
              <li class="<?php echo $visi ?>"><a href="<?php echo base_url('berita') ?>">Visi & Misi</a></li>
            </ul>
          </div>
        </div>
      </nav>

    </div>
  </div>

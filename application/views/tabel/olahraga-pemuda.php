    <!-- Tabels
    ================================================== -->
    <!-- Wrap the rest of the page in another container to center all the content. -->

    <div class="container container-tabel">


      <!-- START THE TABLE -->

      <h1>Olahraga Pemuda</h1>

      <table id="tabel" class="table table-bordered table-hover">
          <thead>
          <tr>
            <th>No</th>
            <th>Nama Organisasi</th>
            <th>Alamat</th>
            <th>Kelurahan</th>
            <th>Jumlah Anggota</th>
            <th>Kegiatan Rutin</th>
            <th>Tahun Terbentuk</th>
            <th>Keterangan</th>
          </tr>
          </thead>
          <tbody>
            <?php
            $i = 1;
            foreach ($data as $row) {
              echo "
                <tr>
                  <td>$i</td>
                  <td>$row->nama</td>
                  <td>$row->alamat</td>
                  <td>$row->kelurahan</td>
                  <td>$row->jum_anggota</td>
                  <td>$row->kegiatan</td>
                  <td>$row->tahun</td>
                  <td>$row->keterangan</td>
                </tr>";
              $i++;
            } ?>
          </tbody>
        </table>

      <hr class="featurette-divider">

      <!-- /END THE FEATURETTES -->

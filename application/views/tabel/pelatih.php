<!-- Tabels
================================================== -->
<!-- Wrap the rest of the page in another container to center all the content. -->

<div class="container container-tabel">


<!-- START THE TABLE -->

<h1>Atlet</h1>

<table id="tabel" class="table table-bordered table-hover">
    <thead>
    <tr>
      <th>No</th>
      <th>Nama</th>
      <th>Cabang Olahraga</th>
      <th>Sertifikasi</th>
      <th>Keterangan</th>
    </tr>
    </thead>
    <tbody>
      <?php
      $i = 1;
      foreach ($data as $row) {
        echo "
          <tr>
            <td>$i</td>
            <td>$row->nama</td>
            <td>$row->cab_olahraga</td>
            <td>$row->sertifikat</td>
            <td>$row->keterangan</td>
          </tr>";
        $i++;
      } ?>
    </tbody>
  </table>

<hr class="featurette-divider">

<!-- /END THE FEATURETTES -->

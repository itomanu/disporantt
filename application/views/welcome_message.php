<!-- Carousel
================================================== -->
<div id="myCarousel" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <?php
    echo "<li data-target=\"#myCarousel\" data-slide-to=\"0\" class=\"active\"></li>";
    $i = 1;
    foreach ($carousel as $row) {
      echo "<li data-target=\"#myCarousel\" data-slide-to=\"$i\"></li>";
      $i++;
    } ?>
  </ol>
  <div class="carousel-inner" role="listbox">
    <div class="item active">
      <img class="first-slide" src="<?php echo base_url('public/img/carousel01.png')?>" alt="First slide">
      <div class="container">
        <div class="carousel-caption">
          <img src="<?php echo base_url('public/img/kupang.png')?>" alt="Logo Kota Kupang">
          <h2 class="title">Dinas Pemuda dan Olahraga</h2>
          <h1 class="title">Kota Kupang</h1>
        </div>
      </div>
    </div>
    <?php
    foreach ($carousel as $row) {
      echo "
      <div class='item'>
        <img class='second-slide' src='".base_url('public/img/'.$row->img)."' alt='Second slide'>
        <div class='container'>
          <div class='carousel-caption'>
            <h1>$row->title</h1>
            <p>$row->subtitle</p>
          </div>
        </div>
      </div>";
    } ?>
  </div>
  <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div><!-- /.carousel -->


<!-- Marketing messaging and featurettes
================================================== -->
<!-- Wrap the rest of the page in another container to center all the content. -->


<div class="container marketing">

  <div class="row">
    <h2 style="text-align: center;margin-bottom: 25px;">BERITA POPULER</h2>
    <div class="row">
      <?php
      foreach ($pop as $row) {
        echo "
          <div class=\"col-md-4\">
            <img class=\"img-responsive center-block\" data-src=\"holder.js/400x200/auto\" alt=\"Generic placeholder image\">
            <h2>$row->judul</h2>
            <p>$row->isi</p>
            <a href=\"".base_url('berita/r/'.$row->nama_id)."\">Selengkapnya...</a>
          </div>";
      } ?>
    </div>

  </div>

  <!-- START THE FEATURETTES -->

  <hr class="featurette-divider">

  <div class="row featurette">
    <div class="col-md-7">
      <h2 class="featurette-heading">Membangun Pemuda Kota Kupang.</h2>
      <p class="lead">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus, tellus ac cursus commodo.</p>
    </div>
    <div class="col-md-5">
      <img class="featurette-image img-responsive center-block" data-src="holder.js/500x500/auto" alt="Generic placeholder image">
    </div>
  </div>

  <hr class="featurette-divider">

  <div class="row featurette">
    <div class="col-md-7 col-md-push-5">
      <h2 class="featurette-heading">Pemuda Yang Berprestasi.</h2>
      <p class="lead">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus, tellus ac cursus commodo.</p>
    </div>
    <div class="col-md-5 col-md-pull-7">
      <img class="featurette-image img-responsive center-block" data-src="holder.js/500x500/auto" alt="Generic placeholder image">
    </div>
  </div>

  <hr class="featurette-divider">

  <div class="row featurette">
    <div class="col-md-7">
      <h2 class="featurette-heading">Pemuda Yang Berjuang Bagi Bangsa dan Negara</h2>
      <p class="lead">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus, tellus ac cursus commodo.</p>
    </div>
    <div class="col-md-5">
      <img class="featurette-image img-responsive center-block" data-src="holder.js/500x500/auto" alt="Generic placeholder image">
    </div>
  </div>

  <hr class="featurette-divider">

  <!-- /END THE FEATURETTES -->
